import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/login/login.dart';
import 'package:ite/ui/root/root_page.dart';
import 'bloc/setting_bloc.dart';
import 'helper/app_constant.dart';
import 'local/app_local.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await settingsBloc.getSettingsPreferences();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    AppConstant.context=context;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    return StreamBuilder<String>(
      stream: settingsBloc.currentLangStream,
      builder: (context, langSnapshot) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blueGrey,
          ),
          supportedLocales:const [Locale('en'), Locale('ar')],
          localizationsDelegates:const [
             AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          locale: Locale(langSnapshot.data??'en'),
          home: const InitPage(),
        );
      }
    );
  }
}

class InitPage extends StatelessWidget {
  const InitPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    AppConstant.screenSize = MediaQuery.of(context).size;
    return const SplashScreen();
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override 
  void initState() {
    super.initState();
    dataStore.init();
    Future.delayed(const Duration(seconds: 3)).then((value)=>_openOnBoard());
  }
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        backgroundColor: Colors.blueGrey,
        body: Center(
          child: CircleAvatar(
            foregroundImage: AssetImage(
              "asset/images/7.jpg",
            ),
            radius: 100,
          ),
        ));
  }
  _openOnBoard(){
    dataStore.getIsFirstTimeLogin().then((value) {
      if(value){
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) =>const LoginPage(),));
      }else{Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) =>const RootPage(),));}

    });
  }
}


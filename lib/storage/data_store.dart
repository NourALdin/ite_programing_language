import 'package:ite/model/app_model.dart';
import 'package:ite/model/product_model.dart';
import 'package:ite/model/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataStore {
  static final DataStore _singletonBloc = DataStore._internal();

  String _text = "";

  late String _lang;

  AppModel? appModel = AppModel();

  UserModel? userModel = UserModel();

  final List<String> proName = [
    "salad",
    "candy",
    "tabollah",
    "kabsah",
    "risebbsalia",
    "moulokhia",
    "mgadara",
    "burger",
    "chicken white",
    "samak",
    "fish",
    "pizza"
  ];

  // UserModel _userModel = UserModel();
  //
  // UserModel get user => _userModel;
  //
  // OtpRequestValueModel _otpRequestValue = OtpRequestValueModel();

  // CountriesModel _countries = CountriesModel(result: Result(data: [CountryItem(name: "")]));
  //
  // CountriesModel get countries => _countries;

  String get text => _text;

  set text(String value) {
    _text = value;
  }

  String get lang => _lang;

  set lang(String value) {
    _lang = value;
  }

  factory DataStore() {
    return _singletonBloc;
  }

  DataStore._internal() {
    getLang().then((onVal) {
      _lang = onVal;
    });

    // getUser().then((onVal) {
    //   _userModel = onVal;
    // });

    // getCountries().then((onVal) {
    //   _countries = onVal;
    // });
  }

  init() {
    appModel?.appProduct = List.generate(
        100,
        (index) => ProductItemModel(
            id: index,
            userID: index % 9,
            productName: proName[index % 10],
            like: index%4,
            numberOfWatch: index%7,
            amount: (index % 10) + 4,
            category: "food",
            expiryDate: DateTime.now().toString(),
            phoneNumber: "+9715${index % 10}34${index % 10}",
            photoPathUrl: "asset/images/${index % 10}.jpg",
            price: 2000,
            discounts: [],
          comments: List.generate(10, (index) => CommentModel(
            userPhoto: "asset/images/${index % 10}.jpg",
            userName: "Nour $index",
            text: "that fantastic $index"
          )).toList()
        )).toList();
    appModel?.userModel = List.generate(
        10,
        (index) => UserModel(
            id: index,
            userName: "Nour $index",
            password: "123456",
            email: "nour$index@gmail.com",
            photoPathUrl: "asset/images/${index % 10}.jpg",
            myProduct: appModel?.appProduct?.where((element) => element?.userID == index).toList()));
    appModel?.userModel?.forEach((element) {
      element?.myProduct?.forEach((element) {});
    });
  }

  deleteProduct({int? productId, int? userId}) {
    userModel?.myProduct?.removeWhere((element) => element?.id == productId && element?.userID == userId);
    appModel?.appProduct?.removeWhere((element) => element?.id == productId && element?.userID == userId);
  }

  updateProduct({int? productId, int? userId, int? amount, String? productName}) {
    userModel?.myProduct?.forEach((element) {
      if (element?.id == productId && element?.userID == userId) {
        element!.productName = productName;
        element.amount = amount;
      }
    });
    appModel?.appProduct?.forEach((element) {
      if (element?.id == productId && element?.userID == userId) {
        element!.productName = productName;
        element.amount = amount;
      }
    });
  }

  increaseNumberOfWatch({int? productId, int? userId}){
    // userModel?.myProduct?.forEach((element) {
    //   if(element?.id == productId && element?.userID == userId){
    //     element?.numberOfWatch=element.numberOfWatch!+1;
    //   }
    // });
    for(int i=0;i<appModel!.appProduct!.length;i++){
      if(appModel!.appProduct![i]?.id == productId && appModel!.appProduct![i]?.userID == userId){
        appModel!.appProduct![i]!.numberOfWatch=appModel!.appProduct![i]!.numberOfWatch!+1;
      }
    }
  }
  addLike({int? productId}){
    // userModel?.myProduct?.forEach((element) {
    //   if(element?.id == productId ){
    //     element?.like=element.like!+1;
    //   }
    // });
    for(int i=0;i<appModel!.appProduct!.length;i++){
      if(appModel!.appProduct![i]?.id == productId ){
        appModel!.appProduct![i]!.like=appModel!.appProduct![i]!.like!+1;
      }
    }
  }

  Future<bool> setLang(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = value;
    return prefs.setString('lang', value);
  }

  Future<String> getLang() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('lang') ?? 'en';
  }

  Future<bool> setIsFirstTimeLogin(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool("isFirstTimeLogin", value);
  }

  Future<bool> getIsFirstTimeLogin() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("isFirstTimeLogin") ?? true;
  }

  clearData() {
    setIsFirstTimeLogin(true);
  }
}

final dataStore = DataStore();

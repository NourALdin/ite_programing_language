import 'package:flutter/material.dart';
import 'package:ite/bloc/setting_bloc.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/storage/data_store.dart';
import 'package:rxdart/rxdart.dart';

import 'custom_widget/custom_switch.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  final BehaviorSubject<bool?> _currentSwitchStatus = BehaviorSubject.seeded(true);

  @override
  void initState() {
    super.initState();
    dataStore.getLang().then((value){
      _currentSwitchStatus.sink.add(value=="ar"?false:true);
    });
  }
  @override
  void dispose() {
    _currentSwitchStatus.close();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[600],
        centerTitle: true,
        title: Text(AppLocalizations.of(context)!.trans("setting")),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              children: [
                const Spacer(),
                StreamBuilder<bool?>(
                  stream: _currentSwitchStatus.stream,
                  builder: (context, switchSnapshot) {
                    if(switchSnapshot.hasData){
                      return FlutterSwitch(
                        value: switchSnapshot.data!,
                        activeText: "en",
                        inactiveText: "ar",
                        inactiveColor: Colors.teal,
                        width: 55.0,
                        height: 25.0,
                        valueFontSize: AppFonts.getMediumFontSize(),
                        toggleSize: 18.0,
                        showOnOff: true,
                        onToggle: (value) {
                          print(value);
                          _currentSwitchStatus.sink.add(value);
                          if (_currentSwitchStatus.value!) {
                            settingsBloc.changeCurrentLang('en');
                          } else {
                            settingsBloc.changeCurrentLang('ar');
                          }
                        },
                      );
                    }
                    else{
                      return Container();
                    }
                  }
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

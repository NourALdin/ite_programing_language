import 'package:flutter/material.dart';
import 'package:ite/helper/app_constant.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/model/product_model.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/manage_product/product_info.dart';
import 'package:ite/ui/manage_product/product_item_card.dart';
import 'package:rxdart/rxdart.dart';

import '../setting.dart';
import 'drawer_body.dart';

class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  final double maxSlide = 225.0;
  //final List<String> proName=["salad","candy","tabollah","kabsah","risebbsalia","moulokhia","mgadara","burger","chicken white","samak","fish","pizza"];
  final _currentProduct = BehaviorSubject<List<ProductItemModel?>?>();
  final _currentProductFilter = BehaviorSubject.seeded("byName");
  late List<ProductItemModel?> myProducts=[];
  late List<ProductItemModel?> mySearchByName=[];
  late List<ProductItemModel?> mySearchByCategory=[];
  late List<ProductItemModel?> mySearchByExpiry=[];
  @override
  void initState() {
    super.initState();
    myProducts=dataStore.appModel!.appProduct!;
    _currentProduct.sink.add(dataStore.appModel?.appProduct);
    _animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 250));
  }

  void toggle() => _animationController.isDismissed ? _animationController.forward() : _animationController.reverse();

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, _) {
        double slide = maxSlide * _animationController.value;
        double scale = 1 - (_animationController.value * 0.3);
        return Stack(
          children: [
            const DrawerBody(),
            Transform(
                transform: Matrix4.identity()
                  ..translate(slide)
                  ..scale(scale),
                alignment: Alignment.centerLeft,
                child: _buildBody())
          ],
        );
      },
    );
  }

  Widget _buildBody() {
    return GestureDetector(
      onTap: () {
        if (!_animationController.isDismissed) {
          toggle();
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Column(
                  children: [
                    AppBar(
                      backgroundColor: Colors.blueGrey,
                      automaticallyImplyLeading: false,
                      actions: [
                        IconButton(
                          onPressed: () {
                            toggle();
                          },
                          icon: const Icon(Icons.menu),
                        ),
                        const Spacer(),
                        // IconButton(onPressed: (){
                        //   Navigator.of(context).push(MaterialPageRoute(builder: (context) =>const SearchPage(),));
                        // }, icon: Icon(Icons.search))
                        IconButton(onPressed: (){
                          showDialog(context: context, builder:(context) {
                            return Dialog(
                              backgroundColor: Colors.transparent,
                              child: Container(
                                height: AppConstant.screenSize.height*0.3,
                                decoration: BoxDecoration(
                                    color: Colors.white70,
                                  borderRadius: BorderRadius.circular(24),
                                        
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("Search By Name",style: TextStyle(
                                      color: Colors.blue
                                    ),),
                                  ],
                                ),
                              ),
                            );
                          },);
                        },icon:const Icon(Icons.filter,size: 20,),),
                        PopupMenuButton(
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(20.0),
                            ),
                          ),
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              child: Text(AppLocalizations.of(context)!.trans("setting")),
                              value: 1,
                            ),
                          ],
                          onSelected: (value) {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => const SettingPage(),
                            ));
                          },
                        ),

                      ],
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height * 0.05,
                      child: Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height * 0.05,
                        decoration: const BoxDecoration(
                          color: Colors.blueGrey,
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
                        ),
                      ),
                    ),
                  ],
                ),
                _buildSearchTextField()
              ],
            ),
            preferredSize: Size(double.infinity, MediaQuery.of(context).size.height * 0.15)),
        body: Padding(
          padding: const EdgeInsets.only(top: 24, left: 8, right: 8),
          child: StreamBuilder<List<ProductItemModel?>?>(
              stream: _currentProduct.stream,
              builder: (context, productSnapshot) {
                if(productSnapshot.hasData){
                  return GridView.builder(
                      gridDelegate:
                      const SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 200, childAspectRatio: 1, crossAxisSpacing: 20, mainAxisSpacing: 20),
                      itemCount: productSnapshot.data?.length,
                      itemBuilder: (BuildContext ctx, index) {
                        return ProductItem(
                          myProduct: productSnapshot.data![index],
                          onTap: () {
                            dataStore.increaseNumberOfWatch(productId:productSnapshot.data![index]?.id ,userId:productSnapshot.data![index]?.userID );
                            showDialog(
                              context: context,
                              builder: (context) {
                                return ProductInfoPage(
                                  myProducts: productSnapshot.data![index],
                                  index: index,
                                );
                              },
                            ).then((value) {
                              setState(() {

                              });
                            });
                          },
                        );
                      });
                }
                return Container();
              }),
        ),
      ),
    );
  }

  Widget _buildSearchTextField() {
    return Container(
        width: MediaQuery.of(context).size.width * 0.8,
        decoration: const BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.black26,
            spreadRadius: 10,
            blurRadius: 20,
          )
        ]),
        child: TextFormField(
          onChanged: (value){
            _searchByName(name: value);
            //_searchByCategory(category: value);
            //_searchByExpiryDate(expiry: value);
          },
            style: const TextStyle(
              color: Colors.black45,
            ),
            decoration: InputDecoration(
              errorStyle: const TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide.none,
              ),
              filled: true,
              fillColor: Colors.white.withOpacity(0.99),
              hintText: AppLocalizations.of(context)?.trans("search"),
              hintStyle: TextStyle(
                fontSize: AppFonts.getMediumFontSize(),
                color: Colors.black45,
              ),
            )));
  }

   _searchByName({String? name})async{
    mySearchByName=[];
    for (var element in myProducts) {
      if(element!.productName!.contains(name!)){
        mySearchByName.add(element);
      }
    }
    _currentProduct.sink.add(mySearchByName);
  }
  _searchByCategory({String? category})async{
    mySearchByCategory=[];
    for (var element in myProducts) {
      if(element!.category!.contains(category!)){
        mySearchByCategory.add(element);
      }
    }
    _currentProduct.sink.add(mySearchByCategory);
  }
  _searchByExpiryDate({String? expiry})async{
    mySearchByExpiry=[];
    for (var element in myProducts) {
      if(element!.expiryDate!.contains(expiry!)){
        mySearchByExpiry.add(element);
      }
    }
    _currentProduct.sink.add(mySearchByExpiry);
  }
}

import 'package:flutter/material.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/manage_product/add_product_dialog.dart';
import '../login/login.dart';
import '../profile_page.dart';

class DrawerBody extends StatefulWidget {
  const DrawerBody({Key? key}) : super(key: key);

  @override
  _DrawerBodyState createState() => _DrawerBodyState();
}

class _DrawerBodyState extends State<DrawerBody> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blueGrey[800],
      body: Padding(
        padding: const EdgeInsets.only(top: 70, bottom: 30, left: 39),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             CircleAvatar(
              foregroundImage: AssetImage(
                dataStore.userModel!.photoPathUrl ?? "asset/images/0.jpg",
              ),
              radius: 70,
            ),
            _buildDrawerItem(context, iconData: Icons.person, textLocalKey: "profile", onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>const ProfilePage(),));
            }),
            _buildDrawerItem(context, iconData: Icons.add, textLocalKey: "add_product", onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return const AddProductDialog();
                  });
            }),
            const Spacer(),
            GestureDetector(
                onTap: () {
                  dataStore.clearData();
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => const LoginPage(),
                  ));
                },
                child: Text(
                  AppLocalizations.of(context)!.trans("logout"),
                  style: TextStyle(color: Colors.white, fontSize: AppFonts.getNormalFontSize()),
                ))
          ],
        ),
      ),
    );
  }

  Widget _buildDrawerItem(BuildContext context, {String? textLocalKey, IconData? iconData, GestureTapCallback? onTap}) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        GestureDetector(
          onTap: onTap,
          child: Container(
            width: MediaQuery.of(context).size.width * 0.4,
            height: 40,
            padding: const EdgeInsets.all(8.0),
            decoration: BoxDecoration(color: Colors.blueGrey, borderRadius: BorderRadius.circular(25)),
            child: Row(
              children: [
                Icon(
                  iconData,
                  size: 25,
                  color: Colors.white,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  AppLocalizations.of(context)!.trans(textLocalKey!),
                  style: TextStyle(color: Colors.white, fontSize: AppFonts.getMediumFontSize()),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ite/helper/app_font.dart';
class MainTextField extends StatefulWidget {
  final String? text;
  final TextInputType? inputType;
  final TextEditingController? controller;
  final FormFieldValidator? validator;
  final Color? color;
  final IconData? icon;
  final bool? enable;
  final bool obscureText;
  final FocusNode? focusNode;
  final Widget? isCodePicker;
  final Function? onChangeText;
  final String? assetIcon;
  final bool? numberOnly;
  final double? hight;
  final double? width;
  final Color? textFieldColor;
  const MainTextField(
      {this.text,
        this.controller,
        this.validator,
        this.color = Colors.grey,
        this.icon,
        this.inputType,
        this.enable = true,
        this.obscureText = false,
        this.focusNode,
        this.isCodePicker,
        this.onChangeText,
        this.assetIcon,
        this.numberOnly = false, this.hight=40, this.textFieldColor=Colors.white70, this.width=double.infinity});

  @override
  _MainTextFieldState createState() => _MainTextFieldState();
}

class _MainTextFieldState extends State<MainTextField> {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height:widget.hight,
          width: widget.width,
          decoration:const BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 1,
                blurRadius: 20
              )
            ]
          ),
          child: TextFormField(
            // inputFormatters: <TextInputFormatter>[
            //   widget.numberOnly ? FilteringTextInputFormatter.digitsOnly : FilteringTextInputFormatter.singleLineFormatter,
            // ],
            onChanged: (value) {
              widget.onChangeText!(value);
            },
            focusNode: widget.focusNode,
            obscureText: widget.obscureText,
            enabled: widget.enable,
            controller: widget.controller,
            validator: widget.validator,
            keyboardType: widget.inputType,
            style:const TextStyle(
              color: Colors.cyan,
            ),
            decoration: InputDecoration(
                errorStyle:const TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  borderSide: BorderSide.none,
                ),
                filled: true,
                fillColor: widget.textFieldColor,
                hintText: widget.text,
                hintStyle: TextStyle(
                  fontSize: AppFonts.getSmallFontSize(),
                  color: widget.color,
                ),)
          ),
        ),
      ],
    );
  }
}

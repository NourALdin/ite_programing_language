import 'package:flutter/material.dart';

class CustomDrawer extends StatefulWidget {
  final  child;
  const CustomDrawer({Key? key, this.child}) : super(key: key);


  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    var drawer=Container(color: Colors.amber,);
    return Stack(
      children: [
        drawer,
        widget.child
      ],
    );
  }
}

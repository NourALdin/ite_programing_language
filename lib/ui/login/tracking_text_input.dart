import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ite/helper/app_font.dart';
import 'input_helper.dart';

typedef CaretMoved = void Function(Offset globalCaretPosition);
typedef TextChanged = void Function(String text);

// Helper widget to track caret position.
class TrackingTextInput extends StatefulWidget {
  final CaretMoved? onCaretMoved;
  final TextChanged? onTextChanged;
  final String? hint;
  final String? label;
  final bool isObscured;
  final Function onChange;

  const TrackingTextInput({Key? key, this.onCaretMoved, this.onTextChanged, this.hint, this.label, this.isObscured=true,required this.onChange}) : super(key: key);
  @override
  _TrackingTextInputState createState() => _TrackingTextInputState();
}

class _TrackingTextInputState extends State<TrackingTextInput> {
  final GlobalKey _fieldKey = GlobalKey();
  final TextEditingController _textController = TextEditingController();
  Timer? _debounceTimer;
  @override
  initState() {
    _textController.addListener(() {
      // We debounce the listener as sometimes the caret position is updated after the listener
      // this assures us we get an accurate caret position.
      if (_debounceTimer?.isActive ?? false) _debounceTimer!.cancel();
      _debounceTimer = Timer(const Duration(milliseconds: 100), () {
        if (_fieldKey.currentContext != null) {
          // Find the render editable in the field.
          final RenderObject fieldBox =
          _fieldKey.currentContext!.findRenderObject() as RenderObject;
          Offset caretPosition = getCaretPosition(fieldBox);

          if (widget.onCaretMoved != null) {
            widget.onCaretMoved!(caretPosition);
          }
        }
      });
      if (widget.onTextChanged != null) {
        widget.onTextChanged!(_textController.text);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Container(
        decoration:const BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                spreadRadius: 1,
                blurRadius: 20
            )
          ]
        ),
        child: TextFormField(
            key: _fieldKey,
            controller: _textController,
            obscureText: widget.isObscured,
            onChanged: (value){
              widget.onChange(value);
            },
            style:const TextStyle(
               color: Colors.black38
            ),
            decoration: InputDecoration(
              errorStyle:const TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide.none,
              ),
              filled: true,
              fillColor: Colors.white,
              hintText: widget.hint,
              hintStyle: TextStyle(
                fontSize: AppFonts.getSmallFontSize(),
                color: Colors.black38,
              ),),
            validator: (value) {}),
      ),
    );
  }
}
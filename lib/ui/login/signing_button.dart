import 'package:flutter/material.dart';

class SigningButton extends StatelessWidget {
  final Widget? child;
  final Gradient? gradient;
  final double? width;
  final double? height;
  final GestureTapCallback? onPressed;
  final Color? color;

  const SigningButton({Key? key, this.gradient, this.width, this.height, this.onPressed, this.child, this.color=Colors.blueGrey}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 50.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25.0),
          color: color),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: onPressed,
            child: Center(
              child: child,
            )),
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/helper/validator.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/login/signing_button.dart';
import 'package:ite/ui/login/teddy_controller.dart';
import 'package:ite/ui/login/tracking_text_input.dart';
import 'package:ite/ui/register/singup.dart';
import 'package:ite/ui/root/root_page.dart';
import 'package:rxdart/rxdart.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TeddyController? _teddyController;
  final _currentEmailBehavior=BehaviorSubject.seeded("email");
  final _currentPasswordBehavior=BehaviorSubject.seeded("1111");
  final _currentButtonColor=BehaviorSubject.seeded(Colors.blueGrey);
  @override
  initState() {
    _teddyController = TeddyController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets devicePadding = MediaQuery.of(context).padding;

    return Scaffold(
      backgroundColor:const Color.fromRGBO(93, 142, 155, 1.0),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
              child: Container(
                decoration:const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    stops: [0.0, 1.0],
                    colors: [
                      Colors.white70,
                      Colors.blueGrey
                      // Color.fromRGBO(170, 207, 211, 1.0),
                      // Color.fromRGBO(93, 142, 155, 1.0),
                    ],
                  ),
                ),
              )),
          Positioned.fill(
            child: SingleChildScrollView(
                padding: EdgeInsets.only(
                    left: 20.0, right: 20.0, top: devicePadding.top + 50.0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          height: 200,
                          padding: const EdgeInsets.only(left: 30.0, right:30.0),
                          child: FlareActor(
                            "asset/flare/Teddy.flr",
                            shouldClip: false,
                            alignment: Alignment.bottomCenter,
                            fit: BoxFit.contain,
                            controller: _teddyController,
                          )),
                      Container(
                          decoration:const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                              BorderRadius.all(Radius.circular(25.0))),
                          child: Padding(
                            padding: const EdgeInsets.all(30.0),
                            child: Form(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    TrackingTextInput(
                                        label: AppLocalizations.of(context)!.trans("email"),
                                        hint: AppLocalizations.of(context)!.trans("what_is_your_name"),
                                        isObscured: false,
                                        onChange: (value){
                                          _currentEmailBehavior.sink.add(value);
                                        },
                                        onCaretMoved: (Offset caret) {
                                          _teddyController!.coverEyes(caret == null);
                                          _teddyController!.lookAt(caret);
                                        }),
                                    TrackingTextInput(
                                      label: AppLocalizations.of(context)!.trans("password"),
                                      hint: AppLocalizations.of(context)!.trans("try_pass"),
                                      isObscured: true,
                                      onChange: (value){
                                        _currentPasswordBehavior.sink.add(value);
                                      },
                                      onCaretMoved: (Offset caret) {
                                        _teddyController!.coverEyes(caret != null);
                                        //TODO was null
                                        _teddyController!.lookAt(Offset.zero);
                                      },
                                      onTextChanged: (String value) {
                                        _teddyController!.setPassword(value);
                                      },
                                    ),
                                    StreamBuilder<MaterialColor>(
                                      stream: _currentButtonColor.stream,
                                      builder: (context, colorSnapshot) {
                                        return SigningButton(
                                          color: colorSnapshot.data,
                                            child: Text(AppLocalizations.of(context)!.trans("login"),
                                                style:const TextStyle(
                                                    fontFamily: "RobotoMedium",
                                                    fontSize: 16,
                                                    color: Colors.white)),
                                            onPressed: () {
                                              _teddyController!.submitPassword();
                                              if(emailValidator(email: _currentEmailBehavior.value)==null && passwordValidator(password: _currentPasswordBehavior.value)==null){
                                                _checkIfRegister().then((value){
                                                  print(value);
                                                  if(value){
                                                    dataStore.setIsFirstTimeLogin(false);
                                                    Future.delayed(const Duration(seconds: 3)).then((value){
                                                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) =>const RootPage(),));
                                                    });
                                                  }
                                                  else{
                                                    _currentButtonColor.sink.add(Colors.pink);
                                                    Future.delayed(const Duration(seconds: 4)).then((value){
                                                      _currentButtonColor.sink.add(Colors.blueGrey);
                                                    } );
                                                  }
                                                });
                                              }
                                              else{
                                                _currentButtonColor.sink.add(Colors.pink);
                                                Future.delayed(const Duration(seconds: 4)).then((value){
                                                  _currentButtonColor.sink.add(Colors.blueGrey);
                                                } );
                                              }
                                            });
                                      }
                                    ),
                                    const SizedBox(height: 10,),
                                    Text(AppLocalizations.of(context)!.trans("do_not_have_an_account"),style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontSize: AppFonts.getSmallFontSize(),
                                    ),),
                                    GestureDetector(
                                      onTap: (){
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) =>const SingIn(),));
                                      },
                                      child: Text(AppLocalizations.of(context)!.trans("register_now"),style: TextStyle(
                                        color: Colors.blueGrey[600],
                                        fontSize: AppFonts.getSmallFontSize(),
                                      )),
                                    )
                                  ],
                                )),
                          )),
                    ])),
          ),
        ],
      ),
    );
  }
  Future<bool> _checkIfRegister() async {
    bool temp=false;
    dataStore.appModel?.userModel?.forEach((element) {
      print(element?.email);
      if(element?.email==_currentEmailBehavior.value){
        print("i'm here");
        dataStore.userModel=element;
        temp=true;
        return;
      }
    });
    return temp;
  }
}
import 'package:flutter/material.dart';
import 'package:ite/helper/app_constant.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/helper/validator.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/model/user_model.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/custom_widget/custom_text_field.dart';
import 'package:ite/ui/login/login.dart';
import 'package:ite/ui/root/root_page.dart';

import '../login/signing_button.dart';

class SingIn extends StatefulWidget {
  const SingIn({Key? key}) : super(key: key);

  @override
  _SingInState createState() => _SingInState();
}

class _SingInState extends State<SingIn> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var devicePadding = MediaQuery.of(context).padding;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.blueGrey[400],
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
            padding: EdgeInsets.only(left: 20.0, right: 20.0, top: devicePadding.top + 100.0),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    width: 15,
                  ),
                  Text(
                    AppLocalizations.of(context)!.trans("register"),
                    style: TextStyle(
                        color: Colors.blueGrey[800], fontSize: AppFonts.getXXXLargeFontSize(), fontWeight: FontWeight.bold, fontFamily: "RobotoMedium"),
                  ),
                ],
              ),
              SizedBox(
                height: AppConstant.screenSize.height * 0.1,
              ),
              Container(
                  decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(25.0))),
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Column(
                      children: <Widget>[
                        MainTextField(
                          controller: _userNameController,
                          onChangeText: (value){},
                          text: AppLocalizations.of(context)!.trans("user_name"),
                          hight: AppConstant.screenSize.height * 0.07,
                          validator: (value) {
                            return nameValidator(name: value);
                          },
                        ),
                        SizedBox(
                          height: AppConstant.screenSize.height * 0.02,
                        ),
                        MainTextField(
                          controller: _emailController,
                          onChangeText: (value){},
                          text: AppLocalizations.of(context)!.trans("email"),
                          hight: AppConstant.screenSize.height * 0.07,
                          inputType: TextInputType.emailAddress,
                          validator: (value) {
                            return emailValidator(email: value);
                          },
                        ),
                        SizedBox(
                          height: AppConstant.screenSize.height * 0.02,
                        ),
                        MainTextField(
                          controller: _passwordController,
                          onChangeText: (value){},
                          hight: AppConstant.screenSize.height * 0.07,
                          text: AppLocalizations.of(context)!.trans("password"),
                          validator: (value) {
                            return passwordValidator(password: value);
                          },
                        ),
                        SizedBox(
                          height: AppConstant.screenSize.height * 0.02,
                        ),
                        MainTextField(
                          controller: _confirmPasswordController,
                          onChangeText: (value){},
                          hight: AppConstant.screenSize.height * 0.07,
                          text: AppLocalizations.of(context)!.trans("confirm_password"),
                          validator: (value) {
                            return confirmPasswordValidator(password: _passwordController.value.text, confirmPassword: value);
                          },
                        ),
                        SizedBox(
                          height: AppConstant.screenSize.height * 0.02,
                        ),
                        SigningButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _formKey.currentState!.save();
                              dataStore.setIsFirstTimeLogin(false);
                              dataStore.userModel=UserModel(
                                userName: _userNameController.value.text,
                                email: _emailController.value.text,
                                id: dataStore.appModel?.appProduct?.length,
                                photoPathUrl: "asset/images/0.jpg",
                                password:"123456"
                              );
                              dataStore.appModel?.userModel?.add(dataStore.userModel);
                              Navigator.of(context).pushReplacement(MaterialPageRoute(
                                builder: (context) => const RootPage(),
                              ));
                            }
                          },
                          child: Text(AppLocalizations.of(context)!.trans("sing_in"),
                              style: TextStyle(fontFamily: "RobotoMedium", fontSize: AppFonts.getMediumFontSize(), color: Colors.white)),
                        ),
                        SizedBox(
                          height: AppConstant.screenSize.height * 0.02,
                        ),
                        Text(AppLocalizations.of(context)!.trans("have_an_account"),
                            style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: AppFonts.getSmallFontSize(),
                            )),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushReplacement(MaterialPageRoute(
                              builder: (context) => const LoginPage(),
                            ));
                          },
                          child: Text(AppLocalizations.of(context)!.trans("login"),
                              style: TextStyle(
                                color: Colors.blueGrey,
                                fontSize: AppFonts.getMediumFontSize(),
                              )),
                        )
                      ],
                    ),
                  )),
            ])),
      ),
    );
  }
}

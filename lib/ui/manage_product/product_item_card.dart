import 'package:flutter/material.dart';
import 'package:ite/model/product_model.dart';
import 'package:ite/storage/data_store.dart';

class ProductItem extends StatefulWidget {
  final ProductItemModel? myProduct;
  final GestureTapCallback? onTap;
  const ProductItem({Key? key, this.onTap,required this.myProduct}) : super(key: key);

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        decoration: BoxDecoration(
                          color: Colors.black45,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text("👀 ${widget.myProduct?.numberOfWatch}",
                          style:const TextStyle(color: Colors.white),)),
                    Container(
                        decoration: BoxDecoration(
                          color: Colors.black45,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text("🍟${widget.myProduct?.amount}",
                          style:const TextStyle(color: Colors.white),)),
                  ],
                ),

              ),
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.only(left: 8.0,right: 8.0,bottom:5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width*0.2,
                    height: 30,
                    decoration: BoxDecoration(
                        color: Colors.black45,
                        borderRadius: BorderRadius.circular(90)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(widget.myProduct!.productName!,
                        style:const TextStyle(color: Colors.white),),
                    ),
                  ),
                  GestureDetector(
                    onTap: (){
                      print("like");
                      dataStore.addLike(productId:widget.myProduct?.id );
                      setState(() {

                      });
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.black45,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text("👍 ${widget.myProduct?.like}",
                          style:const TextStyle(color: Colors.white),)),
                  ),
                ],
              ),
            )
          ],
        ),
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(widget.myProduct!.photoPathUrl!)
            ) ,
            borderRadius: BorderRadius.circular(25)),
      ),
    );
  }
}

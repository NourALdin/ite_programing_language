import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ite/helper/app_constant.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/model/product_model.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/custom_widget/custom_text_field.dart';
import 'package:ite/ui/login/signing_button.dart';
import 'package:rxdart/rxdart.dart';

class AddProductDialog extends StatefulWidget {
  const AddProductDialog({Key? key, this.isFromUpdate = false, this.productId = 1}) : super(key: key);
  final bool isFromUpdate;
  final int? productId;

  @override
  _AddProductDialogState createState() => _AddProductDialogState();
}

class _AddProductDialogState extends State<AddProductDialog> {
  final TextEditingController _productNameController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _amountController = TextEditingController();
  final TextEditingController _numberOfDiscountController = TextEditingController();
  final TextEditingController _productPriceController = TextEditingController();
  final List<TextEditingController> _discountList = List.generate(6, (index) => TextEditingController());
  String? _category;

  final _currentFileImage = BehaviorSubject<File?>();
  final _currentDate = BehaviorSubject.seeded("Expiry date");
  final _currentDiscountValue = BehaviorSubject.seeded(0);

  Future getImage(String type) async {
    final PickedFile? image = await ImagePicker().getImage(source: type == "camera" ? ImageSource.camera : ImageSource.gallery);
    _currentFileImage.sink.add(File(image!.path));
    //print("this is image-----------------------------------------and this ${_currentFileImage.value.path}");
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _currentDate.sink.add(AppLocalizations.of(context)!.trans("expiry_date"));
  }

  @override
  void dispose() {
    _currentFileImage.close();
    _currentDiscountValue.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white70),
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                widget.isFromUpdate ?  AppLocalizations.of(context)!.trans("update_your_product") : AppLocalizations.of(context)!.trans("add_your_product"),
                style: const TextStyle(fontWeight: FontWeight.w700, color: Colors.black45, fontSize: AppFonts.medium_font_size),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: MainTextField(
                        controller: _productNameController,
                        onChangeText: (value) {},
                        text: AppLocalizations.of(context)!.trans("product_name"),
                      )),
                  GestureDetector(
                      onTap: () {
                        _onChangeImageWidgetPressed();
                      },
                      child: Container(
                        decoration: BoxDecoration(boxShadow: const [
                          BoxShadow(color: Colors.black38, spreadRadius: 5, blurRadius: 10),
                        ], borderRadius: BorderRadius.circular(30)),
                        child: StreamBuilder<File?>(
                            stream: _currentFileImage.stream,
                            builder: (context, imagePickerSnapshot) {
                              if (imagePickerSnapshot.hasData) {
                                return CircleAvatar(radius: 40, foregroundImage: FileImage(imagePickerSnapshot.data!));
                              }
                              return const CircleAvatar(radius: 40, foregroundImage: AssetImage("asset/images/0.jpg"));
                            }),
                      )),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              widget.isFromUpdate
                  ? Container()
                  : Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: MainTextField(
                                  controller: _phoneNumberController,
                                  onChangeText: (value) {},
                                  text: AppLocalizations.of(context)!.trans("phone_number"),
                                )),
                          ],
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                      ],
                    ),
              Row(
                children: [
                  widget.isFromUpdate
                      ? Container()
                      : Column(
                          children: [
                            SizedBox(
                                width: MediaQuery.of(context).size.width * 0.2,
                                child: SigningButton(
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return Dialog(
                                          backgroundColor: Colors.transparent,
                                          child: StreamBuilder<int>(
                                              stream: _currentDiscountValue.stream,
                                              builder: (context, dialogHighSnapshot) {
                                                if (dialogHighSnapshot.hasData) {
                                                  return Container(
                                                    height: AppConstant.screenSize.height * double.parse("0.${dialogHighSnapshot.data! + 2}"),
                                                    padding: const EdgeInsets.all(8),
                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white70),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        MainTextField(
                                                          controller: _productPriceController,
                                                          onChangeText: (value) {},
                                                          width: AppConstant.screenSize.width * 0.5,
                                                          text: AppLocalizations.of(context)!.trans("product_price"),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        MainTextField(
                                                          controller: _numberOfDiscountController,
                                                          width: AppConstant.screenSize.width * 0.4,
                                                          numberOnly: true,
                                                          inputType: TextInputType.number,
                                                          text: AppLocalizations.of(context)!.trans("number_of_discount"),
                                                          onChangeText: (String value) {
                                                            if (value.isEmpty) {
                                                              _currentDiscountValue.sink.add(0);
                                                              return;
                                                            }
                                                            if ((int.parse(value) >= 0 && int.parse(value) < 7)) {
                                                              value.isNotEmpty
                                                                  ? _currentDiscountValue.sink.add(int.parse(value))
                                                                  : _currentDiscountValue.sink.add(0);
                                                              return;
                                                            }
                                                          },
                                                        ),
                                                        StreamBuilder<int>(
                                                          stream: _currentDiscountValue.stream,
                                                          builder: (context, discountSnapshot) {
                                                            if (discountSnapshot.hasData) {
                                                              return ListView.builder(
                                                                itemCount: discountSnapshot.data,
                                                                shrinkWrap: true,
                                                                itemBuilder: (context, index) {
                                                                  return Column(
                                                                    children: [
                                                                      const SizedBox(
                                                                        height: 10,
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          MainTextField(
                                                                              controller: _discountList[index],
                                                                              width: AppConstant.screenSize.width * 0.34,
                                                                              onChangeText: (value) {},
                                                                              text: AppLocalizations.of(context)!.trans("days_before_expire")),
                                                                          const SizedBox(
                                                                            width: 5,
                                                                          ),
                                                                          MainTextField(
                                                                            width: AppConstant.screenSize.width * 0.3,
                                                                            onChangeText: (value) {},
                                                                            text: AppLocalizations.of(context)!.trans("discount") + " ${index + 1} ",
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ],
                                                                  );
                                                                },
                                                              );
                                                            }
                                                            return Container();
                                                          },
                                                        )
                                                      ],
                                                    ),
                                                  );
                                                }
                                                return Container();
                                              }),
                                        );
                                      },
                                    );
                                  },
                                  color: Colors.white70,
                                  height: AppConstant.screenSize.height * 0.02,
                                  width: AppConstant.screenSize.width * 0.2,
                                  child: Text(
                                    AppLocalizations.of(context)!.trans("price"),
                                    style: TextStyle(color: Colors.black38, fontSize: AppFonts.getSmallFontSize()),
                                  ),
                                )),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.2,
                      child: MainTextField(
                        controller: _amountController,
                        onChangeText: (value) {},
                        hight: AppConstant.screenSize.height * 0.07,
                        text: AppLocalizations.of(context)!.trans("amount"),
                      )),
                  const SizedBox(
                    width: 10,
                  ),
                  widget.isFromUpdate
                      ? Container()
                      : SizedBox(
                          width: MediaQuery.of(context).size.width * 0.23,
                          height: AppConstant.screenSize.height * 0.07,
                          child: DropdownButtonFormField<String>(
                            style: TextStyle(
                              color: Colors.black38,
                              fontSize: AppFonts.getMediumFontSize(),
                            ),
                            dropdownColor: Colors.white70,
                            items: {"food", "NaoH", "H3O"}.map((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            onChanged: (String? value) {
                              _category = value;
                            },
                            decoration: InputDecoration(
                              errorStyle: const TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
                              filled: true,
                              fillColor: Colors.white70,
                              hintText: AppLocalizations.of(context)!.trans("category"),
                              hintStyle: TextStyle(
                                height: 0.8,
                                color: Colors.black38,
                                fontSize: AppFonts.getMediumFontSize(),
                              ),
                              border: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(25.0)),
                            ),
                            isExpanded: true,
                            iconSize: 0,
                          )),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              widget.isFromUpdate
                  ? Container()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        StreamBuilder<String?>(
                            stream: _currentDate.stream,
                            builder: (context, dateTimeSnapshot) {
                              if (dateTimeSnapshot.hasData) {
                                return SigningButton(
                                  onPressed: () {
                                    showDatePicker(
                                      context: context,
                                      initialDate: DateTime.parse("1999-06-25"),
                                      firstDate: DateTime.parse("1999-06-25"),
                                      lastDate: DateTime.parse("2040-06-25"),
                                    ).then((dateValue) {
                                      _currentDate.sink.add(dateValue!.toString().substring(0, 10));
                                    });
                                  },
                                  color: Colors.white70,
                                  height: AppConstant.screenSize.height * 0.01,
                                  width: AppConstant.screenSize.width * 0.2,
                                  child: Text(
                                    dateTimeSnapshot.data!,
                                    style: TextStyle(color: Colors.black38, fontSize: AppFonts.getSmallFontSize()),
                                  ),
                                );
                              }
                              return Container();
                            }),
                      ],
                    ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () {
                        if (widget.isFromUpdate) {
                          dataStore.updateProduct(
                              userId: dataStore.userModel?.id, productId: widget.productId, amount: int.parse(_amountController.value.text), productName: _productNameController.value.text);
                          Navigator.of(context).pop();Navigator.of(context).pop();
                        } else {
                          dataStore.appModel?.appProduct?.add(ProductItemModel(
                              id: dataStore.appModel?.appProduct?.length,
                              userID: dataStore.userModel?.id,
                              productName: _productNameController.value.text,
                              phoneNumber: _phoneNumberController.value.text,
                              numberOfWatch:0,
                              price: double.parse(_productPriceController.value.text),
                              amount: int.parse(_amountController.value.text),
                              category: _category,
                              comments: [],
                              like: 0,
                              photoPathUrl: "asset/images/0.jpg",
                              expiryDate: _currentDate.value,
                              discounts: [
                                int.parse(_discountList[0].value.text.isEmpty ? "0" : _discountList[0].value.text),
                                int.parse(_discountList[1].value.text.isEmpty ? "0" : _discountList[1].value.text),
                                int.parse(_discountList[2].value.text.isEmpty ? "0" : _discountList[2].value.text),
                                int.parse(_discountList[3].value.text.isEmpty ? "0" : _discountList[3].value.text),
                                int.parse(_discountList[4].value.text.isEmpty ? "0" : _discountList[4].value.text),
                                int.parse(_discountList[5].value.text.isEmpty ? "0" : _discountList[5].value.text),
                              ]));
                          dataStore.userModel?.myProduct?.add(ProductItemModel(
                              id: dataStore.appModel?.appProduct?.length,
                              userID: dataStore.userModel?.id,
                              productName: _productNameController.value.text,
                              phoneNumber: _phoneNumberController.value.text,
                              numberOfWatch: 0,
                              price: double.parse(_productPriceController.value.text),
                              amount: int.parse(_amountController.value.text),
                              category: _category,
                              photoPathUrl: "asset/images/0.jpg",
                              expiryDate: _currentDate.value,
                              comments: [],
                              like: 0,
                              discounts: [
                                int.parse(_discountList[0].value.text.isEmpty ? "0" : _discountList[0].value.text),
                                int.parse(_discountList[1].value.text.isEmpty ? "0" : _discountList[1].value.text),
                                int.parse(_discountList[2].value.text.isEmpty ? "0" : _discountList[2].value.text),
                                int.parse(_discountList[3].value.text.isEmpty ? "0" : _discountList[3].value.text),
                                int.parse(_discountList[4].value.text.isEmpty ? "0" : _discountList[4].value.text),
                                int.parse(_discountList[5].value.text.isEmpty ? "0" : _discountList[5].value.text),
                              ]));
                          Navigator.pop(context);
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(color: Colors.blueGrey, borderRadius: BorderRadius.circular(25)),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0, bottom: 8, left: 16, right: 16),
                          child: Text(
                            widget.isFromUpdate ?  AppLocalizations.of(context)!.trans("update") : AppLocalizations.of(context)!.trans("add"),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      )),
                  GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8, left: 16, right: 16),
                        child: Text(
                          AppLocalizations.of(context)!.trans("close"),
                          style: TextStyle(color: Colors.redAccent.withOpacity(0.8)),
                        ),
                      ))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onChangeImageWidgetPressed() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        builder: (context) {
          return Container(
            decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.camera_alt_outlined),
                    title: Text(AppLocalizations.of(context)!.trans("open_camera")),
                    onTap: () {
                      Navigator.pop(context);
                      getImage("camera");
                    }),
                ListTile(
                    leading: const Icon(Icons.camera),
                    title: Text(AppLocalizations.of(context)!.trans("open_gallery")),
                    onTap: () {
                      Navigator.pop(context);
                      getImage("");
                    }),
              ],
            ),
          );
        });
  }
}

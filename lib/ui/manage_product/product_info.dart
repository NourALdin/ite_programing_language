import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ite/helper/app_constant.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/model/product_model.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/custom_widget/custom_text_field.dart';
import 'package:ite/ui/login/signing_button.dart';
import 'package:rxdart/rxdart.dart';

class ProductInfoPage extends StatefulWidget {
  final ProductItemModel? myProducts;
  final int index;

  const ProductInfoPage({Key? key, required this.myProducts, this.index=0}) : super(key: key);

  @override
  _ProductInfoPageState createState() => _ProductInfoPageState();
}

class _ProductInfoPageState extends State<ProductInfoPage> {
  final TextEditingController _messageController=TextEditingController();
  final _currentComment=BehaviorSubject<List<CommentModel?>?>();

  @override
  void initState() {
    super.initState();
    _currentComment.sink.add(widget.myProducts?.comments);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.transparent,
        child: Stack(children: [
          Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.white70),
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.75,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      width: AppConstant.screenSize.width * 0.4,
                      height: AppConstant.screenSize.height * 0.2,
                      decoration: BoxDecoration(
                          image: DecorationImage(fit: BoxFit.cover, image: AssetImage(widget.myProducts!.photoPathUrl!)),
                          borderRadius: BorderRadius.circular(25),
                          boxShadow: const [BoxShadow(color: Colors.black38, blurRadius: 20, spreadRadius: 10)]),
                    ),
                    _buildProductInfo(title:  AppLocalizations.of(context)!.trans("product_name")+" :", text: widget.myProducts!.productName!),
                    _buildProductInfo(title: AppLocalizations.of(context)!.trans("contact_way")+" :", text: widget.myProducts!.phoneNumber.toString()),
                    _buildProductInfo(title: AppLocalizations.of(context)!.trans("category")+" :", text: widget.myProducts!.category!),
                    _buildProductInfo(title: AppLocalizations.of(context)!.trans("expiry_date")+" :", text: widget.myProducts!.expiryDate.toString().substring(0, 19)),
                    _buildProductInfo(title: AppLocalizations.of(context)!.trans("amount")+" :", text: widget.myProducts!.amount.toString()),
                    _buildProductInfo(title: AppLocalizations.of(context)!.trans("price")+" :", text: widget.myProducts!.price.toString()),
                    const SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return Dialog(
                              backgroundColor: Colors.transparent,
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.white70),
                                child: Column(
                                  children: [
                                    Expanded(
                                        child: Container(
                                          padding: const EdgeInsets.all(8),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: StreamBuilder<List<CommentModel?>?>(
                                        stream: _currentComment.stream,
                                        builder: (context, commentsSnapshot) {
                                          if(commentsSnapshot.hasData){
                                            return ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: commentsSnapshot.data?.length,
                                              itemBuilder: (context, index) {
                                                return Column(
                                                  children: [
                                                    const SizedBox(height: 10,),
                                                    Row(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Column(
                                                          children: [
                                                            CircleAvatar(foregroundImage: AssetImage(commentsSnapshot.data![index]!.userPhoto),),
                                                            Text(commentsSnapshot.data![index]!.userName)
                                                          ],
                                                        ),
                                                        const SizedBox(width: 10,),
                                                        Container(
                                                          padding: const EdgeInsets.all(16),
                                                          decoration: BoxDecoration(
                                                              color: Colors.white,
                                                              borderRadius: BorderRadius.circular(30),
                                                              boxShadow:const [
                                                                BoxShadow(
                                                                    color: Colors.black26,
                                                                    blurRadius: 20,
                                                                    spreadRadius: 10
                                                                )
                                                              ]
                                                          ),
                                                          child: Center(child: Text(commentsSnapshot.data![index]!.text)),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                );
                                              },
                                            );
                                          }return Container();
                                        }
                                      ),
                                    )),
                                    const SizedBox(height: 15,),
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: AppConstant.screenSize.width * 0.62,
                                          child: MainTextField(
                                            controller:_messageController,
                                            text: AppLocalizations.of(context)!.trans("enter_your_comment"),
                                            onChangeText: (value) {},
                                          ),
                                        ),
                                        IconButton(onPressed: () {
                                          dataStore.appModel?.appProduct![widget.index]?.comments?.add(CommentModel(
                                            userPhoto: dataStore.userModel!.photoPathUrl as String,
                                            userName: dataStore.userModel!.userName as String,
                                            text: _messageController.value.text
                                          ));
                                          _currentComment.sink.add(dataStore.appModel?.appProduct![widget.index]?.comments);
                                          _messageController.clear();
                                        }, icon:const Icon(Icons.send))
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      },
                      child: Text(
                        AppLocalizations.of(context)!.trans("comment"),
                        style: TextStyle(color: Colors.blue, fontSize: AppFonts.getMediumFontSize(), shadows: const [
                          Shadow(
                            color: Colors.black54,
                            blurRadius: 20,
                          )
                        ]),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SigningButton(
                      onPressed: () {
                        //TODO GO TO WHATSAPP
                      },
                      child: Text(AppLocalizations.of(context)!.trans("bay"), style: TextStyle(fontFamily: "RobotoMedium", fontSize: AppFonts.getMediumFontSize(), color: Colors.white)),
                    )
                  ],
                ),
              ))
        ]));
  }

  Widget _buildProductInfo({String? title, String? text}) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Container(
          height: AppConstant.screenSize.height * 0.05,
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.9),
              borderRadius: BorderRadius.circular(8),
              boxShadow: const [BoxShadow(color: Colors.black12, blurRadius: 20, spreadRadius: 10, offset: Offset(1, 5))]),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(title!, style: TextStyle(fontFamily: "RobotoMedium", fontSize: AppFonts.getMediumFontSize(), color: Colors.black38)),
              Text(text!, style: TextStyle(fontFamily: "RobotoMedium", fontSize: AppFonts.getMediumFontSize(), color: Colors.black54)),
            ],
          ),
        ),
      ],
    );
  }
}

// openWhatsApp() async {
//   var whatsApp = "+963";
//
//   var whatsAppURlAndroid = "whatsapp://send?phone=$whatsApp&text=${Uri.parse("hello")}";
//   var whatAppURLIos = "https://wa.me/$whatsApp?text=${Uri.parse("hello")}";
//   if (Platform.isIOS) {
//     // for iOS phone only
//     if (await canLaunch(whatAppURLIos)) {
//       await launch(whatAppURLIos, forceSafariVC: false);
//     } else {
//     }
//   } else {
//     // android , web
//     print("1111111111");
//     if (await canLaunch(whatsAppURlAndroid)) {
//       await launch(whatsAppURlAndroid);
//     } else {
//     }
//   }
// }
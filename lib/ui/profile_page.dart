import 'package:flutter/material.dart';
import 'package:ite/helper/app_constant.dart';
import 'package:ite/helper/app_font.dart';
import 'package:ite/local/app_local.dart';
import 'package:ite/model/product_model.dart';
import 'package:ite/storage/data_store.dart';
import 'package:ite/ui/manage_product/add_product_dialog.dart';
import 'package:ite/ui/manage_product/product_item_card.dart';

import 'custom_widget/custom_text_field.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.blueGrey[800],
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.only(left: size.width * 0.03, right: size.width * 0.03, top: size.height * 0.1, bottom: size.height * 0.02),
        child: Container(
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(color: Colors.blueGrey, borderRadius: BorderRadius.circular(50)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.arrow_back_outlined,
                  color: Colors.blueGrey[800],
                  size: 25,
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  width: size.width * 0.5,
                  height: size.height * 0.25,
                  decoration: BoxDecoration(
                      image: DecorationImage(fit: BoxFit.cover, image: AssetImage(dataStore.userModel!.photoPathUrl!)),
                      borderRadius: BorderRadius.circular(25),
                      boxShadow: const [BoxShadow(color: Colors.black38, blurRadius: 20, spreadRadius: 10)]),
                ),
              ),
              SizedBox(
                height: size.height * 0.02,
              ),
              MainTextField(
                text: dataStore.userModel!.userName, //AppLocalizations.of(context)!.trans("user_name"),
                hight: 50,
                onChangeText: (value) {},
                textFieldColor: Colors.blueGrey[600],
              ),
              SizedBox(
                height: size.height * 0.025,
              ),
              MainTextField(
                text: dataStore.userModel!.email, //AppLocalizations.of(context)!.trans("email"),
                hight: 50,
                onChangeText: (value) {},
                textFieldColor: Colors.blueGrey[600],
              ),
              SizedBox(
                height: size.height * 0.025,
              ),
              dataStore.userModel!.myProduct != null
                  ? Container(
                      height: AppConstant.screenSize.height * 0.33,
                      width: double.infinity,
                      padding: const EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          color: Colors.white70,
                          borderRadius: BorderRadius.circular(35),
                          boxShadow: const [BoxShadow(color: Colors.black12, blurRadius: 20, spreadRadius: 10)]),
                      child: SingleChildScrollView(
                        controller: _scrollController,
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!.trans("my_product"),
                              style: TextStyle(fontSize: AppFonts.getMediumFontSize(), color: Colors.black54),
                            ),
                            GridView.builder(
                              controller: _scrollController,
                              itemCount: dataStore.userModel!.myProduct!.length,
                              shrinkWrap: true,
                              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 200, childAspectRatio: 1, crossAxisSpacing: 20, mainAxisSpacing: 20),
                              itemBuilder: (context, index) {
                                return ProductItem(
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return Dialog(
                                          child: Container(
                                            width: double.infinity,
                                            height: 100,
                                            padding: const EdgeInsets.all(20),
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Colors.white54),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                const Spacer(),
                                                GestureDetector(
                                                  onTap: () {
                                                    dataStore.deleteProduct(
                                                        userId: dataStore.userModel?.id, productId: dataStore.userModel!.myProduct![index]?.id);
                                                    setState(() {});
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets.all(10),
                                                    decoration:
                                                        BoxDecoration(borderRadius: BorderRadius.circular(40), color: Colors.redAccent.withOpacity(0.8)),
                                                    child: Text(AppLocalizations.of(context)!.trans("delete"),style: TextStyle(color: Colors.white),),
                                                  ),
                                                ),
                                                const Spacer(),
                                                GestureDetector(
                                                  onTap: (){
                                                    showDialog(context: context, builder: (context) {
                                                      return  AddProductDialog(isFromUpdate: true,productId: dataStore.userModel!.myProduct![index]?.id,);
                                                    },);
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets.all(10),
                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(40), color: Colors.blueGrey.withOpacity(0.8)),
                                                    child:  Text(AppLocalizations.of(context)!.trans("update"),style: TextStyle(color: Colors.white),),
                                                  ),
                                                ),
                                                const Spacer()
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  myProduct: ProductItemModel(
                                      userID: dataStore.userModel!.myProduct![index]?.userID,
                                      productName: dataStore.userModel!.myProduct![index]?.productName,
                                      price: dataStore.userModel!.myProduct![index]?.price,
                                      comments:dataStore.userModel?.myProduct![index]?.comments ,
                                      like:dataStore.userModel?.myProduct![index]?.like ,
                                      numberOfWatch:dataStore.userModel!.myProduct![index]?.numberOfWatch,
                                      phoneNumber: dataStore.userModel?.myProduct![index]?.phoneNumber,
                                      id: dataStore.userModel?.myProduct![index]?.id,
                                      category: dataStore.userModel?.myProduct![index]?.category,
                                      expiryDate: dataStore.userModel?.myProduct![index]?.expiryDate,
                                      photoPathUrl: dataStore.userModel!.myProduct![index]?.photoPathUrl,
                                      amount: dataStore.userModel!.myProduct![index]?.amount,
                                      discounts: []),
                                );
                              },
                            )
                          ],
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}

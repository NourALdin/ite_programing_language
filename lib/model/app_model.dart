import 'package:ite/model/product_model.dart';
import 'package:ite/model/user_model.dart';

class AppModel {
  AppModel({this.userModel,this.appProduct});
  List<UserModel?>? userModel;
  List<ProductItemModel?>? appProduct;

}


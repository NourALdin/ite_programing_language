import 'package:ite/model/product_model.dart';

class UserModel {
  UserModel({this.id,this.userName,this.photoPathUrl,this.myProduct,this.email,this.password});
  int? id;
  String? userName;
  String? password;
  String? photoPathUrl;
  String? email;
  List<ProductItemModel?>? myProduct;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    id: json["id"] == null ? null : json["id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
  };

  Map<String, dynamic> toJsonForMessages() => {
    "id": id == null ? null : id,
  };
}


import 'dart:convert';

class ProductItemModel {
  ProductItemModel({this.id,required this.userID,this.like,this.productName,this.photoPathUrl,this.expiryDate,this.category,this.amount,required this.numberOfWatch,this.phoneNumber,this.price,required this.discounts,required this.comments});
  int? id;
  int?userID;
  String? productName;
  String? photoPathUrl;
  String? expiryDate;
  String? category;
  String? phoneNumber;
  int? amount;
  double? price;
  int? numberOfWatch;
  int? like;
  List<int> discounts;
  List<CommentModel?>? comments;
  // factory ProductItemModel.fromJson(Map<String, dynamic> json) => ProductItemModel(
  //   id: json["id"] == null ? null : json["id"],
  // );
  //
  // Map<String, dynamic> toJson() => {
  //   "id": id == null ? null : id,
  // };
  //
  // Map<String, dynamic> toJsonForMessages() => {
  //   "id": id == null ? null : id,
  // };
}
class CommentModel{
  CommentModel({this.userPhoto="",this.userName="",this.text=""});
  String userPhoto;
  String userName;
  String text;
}


import 'package:dio/dio.dart';
import 'package:ite/helper/app_constant.dart';
import 'package:ite/helper/utils.dart';
import 'package:ite/model/error_model.dart';

class ApiProvider {
  Map<String, String> headers = {
    'Content-Type': 'multipart/form-data',
    'Accept': 'application/json',
  };

  static const String?  _loginURL = '${AppConstant.apiBaseUrl}loginMobile';



  // Future<GetTextMobileModel> getTextMobile({String type, @required BuildContext context}) async {
  //   var data = await getMultiPartCallApi(_getTextMobileURL + "/Type:NationalDay", context);
  //   try {
  //     return GetTextMobileModel.fromJson(data);
  //   } catch (e) {
  //     throw data;
  //   }
  // }



  ////////// *****  Main Functions  ***** //////////

  deleteMultiPartCallApi(
      url,
      body,
      ) async {
    ErrorsModel v;
    print(url);
    Utils.instance.removeNullMapObjects(body);
    // if (dataStore.user.token != null) {
    //   print(" is :Bearer  " + dataStore.user.token);
    //   headers['Authorization'] = "Bearer " + dataStore.user.token.toString();
    // } else {
    //   headers['Authorization'] = null;
    // }
    Dio dio =  Dio();
    var data;

    await dio.delete(url, options: Options(headers: headers), data: body).then((val) {
      print(val.data);
      data = val.data;
    }).catchError((e) {
      print(e.response.statusCode);
      print(e.response.data);
      print(e);

      v = ErrorsModel.fromJson(e.response.data);
//      print(e.response.data);
      throw v;
    });
    return data;
  }

  getMultiPartCallApi(url, context) async {
    ErrorsModel v;
    print(url);
    // if (dataStore.user.token != null) {
    //   print(" is :Bearer  " + dataStore.user.token);
    //   headers['Authorization'] = "Bearer " + dataStore.user.token.toString();
    // } else {
    //   headers['Authorization'] = null;
    // }
    Dio dio =  Dio();
     var data;
    await dio
        .get(url,
        options: Options(
          headers: headers,
        ))
        .then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
    }).catchError((e) {
      print(e.response.statusCode);
      print(e.response.data);
      try {} catch (e) {}
      v = ErrorsModel.fromJson(e.response.data);
      // if (v.result?.statusCode == 401) {
      //   dataStore.clearStoredData();
      //   Utils.instance.showToast(text: "you have login from another device please login to continue");
      //   Navigator.pushAndRemoveUntil(
      //       context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()), (Route<dynamic> route) => false);
      // }

//      print(e.response.data);
      throw v;
    });
    return data;
  }

  postMultiPartCallApi(
      url,
      body,
      context,
      ) async {
    ErrorsModel v;
    print(url);
    print(body);
    print(headers);
    Utils.instance.removeNullMapObjects(body);
    // if (dataStore.user.token != null) {
    //   print("is :Bearer " + dataStore.user.token.toString());
    //   headers['Authorization'] = "Bearer " + dataStore.user.token.toString();
    // } else {
    //   headers['Authorization'] = null;
    // }

    Dio dio = new Dio();
    var data;
    await dio.post(url, data: body, options: Options(headers: headers)).then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
    }).catchError((e) {
      print("______________________ error");
      print(e.response.statusCode);

      print(e.response.data);

      print("!!!!!!!!!!!!!!!!!11");
      print(e);
      v = ErrorsModel.fromJson(e.response.data);
      // if (e.response.statusCode == 401) {
      //   dataStore.clearStoredData();
      //   Utils.instance.showToast(text: "you have login from another device please login to continue");
      //   Navigator.pushAndRemoveUntil(
      //       context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()), (Route<dynamic> route) => false);
      // }

      throw v;
//      print(e.response.data);
    });
    return data;
  }

  putMultiPartCallApi(
      url,
      body,
      ) async {
    ErrorsModel v;
    print(url);
    Utils.instance.removeNullMapObjects(body);
    // if (dataStore.user.token != null) {
    //   print(" is :Bearer  " + dataStore.user.token);
    //   headers['Authorization'] = "Bearer " + dataStore.user.token.toString();
    // } else {
    //   headers['Authorization'] = "";
    // }
    print(body);
    Dio dio = new Dio();
    var data;
    try {
      await dio.put(url, data: body, options: Options(headers: headers)).then((val) {
        print("______________________ response");
        print(val.data);
        data = val.data;
      }).catchError((e) {
        // print("______________________ error");
        // print(e.response.statusCode);
        // print(e.response.data);
        //
        // print(e);

        v = ErrorsModel.fromJson(e.response.data);
//      print(e.response.data);
        throw v;
      });
    } catch (e) {
      print(e);
    }

    return data;
  }
}


final apiProvider = ApiProvider();

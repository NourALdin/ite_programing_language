import 'package:flutter/material.dart';
import 'package:ite/storage/data_store.dart';
import 'package:rxdart/rxdart.dart';

class SettingsBloc {
  factory SettingsBloc() {
    return _settingsBloc;
  }

  SettingsBloc._internal();

  static final SettingsBloc _settingsBloc = SettingsBloc._internal();

  final BehaviorSubject<String> _currentLangController = BehaviorSubject<String>();

  ValueStream<String> get currentLangStream => _currentLangController.stream;

  // final BehaviorSubject<Map<String,MaterialColor>> _currentColorController = BehaviorSubject.seeded(
  //     {
  //       "primary":Colors.blueGrey,
  //       "swatch":Colors.blueGrey[800] as MaterialColor
  //     });
  //
  // ValueStream<Map<String,MaterialColor>> get _currentColorStream => _currentColorController.stream;

  Future<void> getSettingsPreferences() async {
    await dataStore.getLang().then((String value) {
      if (value != null) {
        changeCurrentLang(value);
      }
    });
  }

  void changeCurrentLang(String lang) {
    _currentLangController.sink.add(lang);
    dataStore.setLang(lang);
  }

  // Colors getAppColor(){
  //   return Colors.blueGrey as Colors;
  // }

}

final SettingsBloc settingsBloc = SettingsBloc();

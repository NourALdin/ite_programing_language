import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ite/bloc/setting_bloc.dart';

import '../storage/data_store.dart';

class AppLocalizations {
  AppLocalizations(this.locale);

  final Locale locale;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  late Map<String, String> _sentences;

  Future<bool> load() async {
    String data = await rootBundle.loadString('asset/lang/${locale.languageCode}.json');
    Map<String, dynamic> _result = json.decode(data);
    _sentences = {};
    _result.forEach((String key, dynamic value) {
      _sentences[key] = value.toString();
    });

    return true;
  }

  String trans(String key) {
    return _sentences[key] ?? '';
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ar'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) async {
    // Check if user has set the lang or get the lang from system
    AppLocalizations localizations;
    if (dataStore.lang != null) {
      localizations = AppLocalizations(Locale(dataStore.lang));
    } else {
      localizations = AppLocalizations(locale);
      settingsBloc.changeCurrentLang(localizations.locale.languageCode);
    }

    await localizations.load();

    // print("Load ${localizations.locale.languageCode}");

    return localizations;
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}

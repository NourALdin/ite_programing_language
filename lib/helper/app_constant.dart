import 'package:flutter/cupertino.dart';

class AppConstant {
  static BuildContext? context;
  static Size screenSize = MediaQuery.of(context!).size;
  static const String? apiBaseUrl = localUrl;

  static const String localUrl = "http://192.168.5.208:8000/api/";

}

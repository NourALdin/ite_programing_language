import 'package:flutter/cupertino.dart';

 passwordValidator({String? password}) {
  if (password==null) {
    return "Please enter your password";
  } else if (password.length < 6) {
    return "Password must be at least 6 characters";
  }
}

 emailValidator({String? email}) {
  const Pattern pattern = r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
      r'{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]'
      r'{0,253}[a-zA-Z0-9])?)*$';
  final RegExp regex = RegExp(pattern as String);

  if (email==null) {
    return "Please enter your email or phone";
  } else if (!regex.hasMatch(email)){
    return "Invalid email";
  }
}

 emailCreateValidator({String? email, BuildContext? context}) {
  const Pattern pattern = r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
      r'{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]'
      r'{0,253}[a-zA-Z0-9])?)*$';
  final RegExp regex = RegExp(pattern as String);

  if (email==null) {
    return "Please enter your email";
  } else if (!regex.hasMatch(email)){return "Incorrect credentials";}
}

 confirmPasswordValidator({String? password, String? confirmPassword}) {
  if (confirmPassword==null) {
    return "Please enter your password";
  } else if (confirmPassword.length < 6) {
    return "Password must be at least 6 characters";
  } else if (password != confirmPassword) {
    return "Passwords don't match";
  }
}

 nameValidator({
  String? name,
}) {
  if (name!.isEmpty) {
    return "Please enter your username";
  } else if (name.length < 3) {
    return "Username must be at least 3 characters";
  }
}

 phoneValidator({String? phone}) {
  const String arPattern = r'[0-9|٠-٩]';
  final RegExp regExp = RegExp(arPattern);
  if (phone!.isEmpty) {
    return "Please enter your phone number";
  } else if (!regExp.hasMatch(phone) || phone.length < 9) {
    return "Invalid phone number";
  }
}

 phoneCreateValidator({String? phone}) {
  const String arPattern = r'[0-9|٠-٩]';
  final RegExp regExp = RegExp(arPattern);
  if (phone!.isEmpty) {
    return "Please enter your phone number";
  } else if (!regExp.hasMatch(phone) || phone.length < 9) {
    return "Invalid phone number";
  }
}



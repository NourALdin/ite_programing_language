import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


import 'app_constant.dart';

class Utils {
  factory Utils() {
    return _utils;
  }

  Utils._internal();

  static final Utils _utils = Utils._internal();

  static Utils get instance => _utils;

  bool checkFieldIsNotEmpty(TextEditingController controller) {
    if (controller.value.text.replaceAll(' ', '').isNotEmpty)
      return true;
    else
      return false;
  }



  // Duration minusDate({String expiry}) {
  //   DateTime dateTime = DateTime.parse(expiry);
  //   return dateTime.difference(DateTime.now());
  // }
  //
  // Map<String, String> constructTime(int seconds) {
  //   int day = seconds != null ? seconds ~/ 86400 : 1;
  //   int hour = seconds != null ? seconds ~/ 3600 : 1;
  //   int minute = seconds != null ? seconds % 3600 ~/ 60 : 1;
  //   int second = seconds != null ? seconds % 60 : 1;
  //   return {"day": formatTime(day), "hour": formatTime(hour), "minute": formatTime(minute), "second": formatTime(second)};
  // }
  //
  // String formatTime(int timeNum) {
  //   return timeNum < 10 ? "0" + timeNum.toString() : timeNum.toString();
  // }



  bool isNumeric(String result) {
    if (result == null) {
      return false;
    }
    return double.tryParse(result) != null;
  }

  // void showToast({String text}) {
  //   Fluttertoast.showToast(
  //     msg: (text),
  //   );
  // }

  // void showLongToast({String text}) {
  //   Fluttertoast.showToast(
  //     toastLength: Toast.LENGTH_LONG,
  //     msg: (text),
  //   );
  // }

  removeNullMapObjects(Map map) {
    map.removeWhere((key, value) => key == null || value == null);
  }
  //
  // Future<String> getAppVersion() async {
  //   final PackageInfo packageInfo = await PackageInfo.fromPlatform();
  //   return packageInfo.version;
  // }
  // Future<bool> checkCameraPermissionStatus() async {
  //   var isPermissionGranted = false;
  //   var cameraStatus = await Permission.camera.request();
  //   var micStatus = await Permission.microphone.request();
  //   if (cameraStatus == PermissionStatus.granted && micStatus == PermissionStatus.granted) {
  //     isPermissionGranted = true;
  //   } else {
  //     isPermissionGranted = false;
  //   }
  //   return isPermissionGranted;
  // }

  String enumToString(Object o) {
    return o.toString().split('.').last;
  }

//  T stringToEnum<T>(String key, List<T> values) => values.firstWhere((v) => key == enumToString(v), orElse: () => null);

  // copyText({
  //   @required String value,
  // }) {
  //   Clipboard.setData(new ClipboardData(text: value));
  //   showToast(text: "Link copied to clipboard");
  // }

}
